const Post = require('../models/Post');

exports.getPost = (req, res) => {
  res.render('post/post', {
    title: 'Post'
  });
};
exports.postSubmit = (req, res, next) => {
  /*const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/post');
  }*/
  console.log('done');

  	const post = new Post({
    body: req.body.content,
    postedby: req.body.username
  });
    post.save((err) => {
      if (err) { return next(err); }
      req.logIn(post, (err) => {
        if (err) {
          return next(err);
        }else{
	        return res.redirect('/post');
        }
      });
    });
};
