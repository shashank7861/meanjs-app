const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
  body: String,
  postedby: String
  });

const Post = mongoose.model('Post', postSchema);

module.exports = Post;
